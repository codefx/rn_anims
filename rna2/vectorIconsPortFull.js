import AntIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import FeatIcon from 'react-native-vector-icons/Feather';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import Fa5Icon from 'react-native-vector-icons/FontAwesome5';
import FounIcon from 'react-native-vector-icons/Foundation';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import OctIcon from 'react-native-vector-icons/Octicons';
import SimplineIcon from 'react-native-vector-icons/SimpleLineIcons';
import ZocIcon from 'react-native-vector-icons/Zocial';

export {
    AntIcon,
    EntypoIcon,
    EvilIcon,
    FeatIcon,
    FaIcon,
    Fa5Icon,
    MIcon,
    MCIcon,
    FounIcon,
    FontistoIcon,
    IonIcon,
    OctIcon,
    SimplineIcon,
    ZocIcon
};
