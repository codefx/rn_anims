import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Content = ({item, SPACING}) => {
  return (
    <>
      <Text style={styles.text1} numberOfLines={1} adjustsFontSizeToFit>
        {item.title}
      </Text>
      <Text style={styles.subtitle}>{item.subtitle}</Text>
      <View style={{...styles.mainView, marginTop: SPACING}}>
        <Text style={styles.price}>{item.price}</Text>
        <Text style={styles.currency}>USD</Text>
      </View>
    </>
  );
};

export default Content;

const styles = StyleSheet.create({
  text1: {
    textAlign: 'center',
    fontWeight: '800',
    fontSize: 16,
    textTransform: 'uppercase',
  },
  subtitle: {fontSize: 12, opacity: 0.4},
  price: {
    fontSize: 42,
    letterSpacing: 3,
    fontWeight: '900',
    marginRight: 8,
  },
  currency: {
    fontSize: 16,
    lineHeight: 36,
    fontWeight: '800',
    alignSelf: 'flex-end',
  },
  mainView: {flexDirection: 'row'},
});
