import React, {useRef, useState} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  Pressable,
  Animated,
  View,
  Image,
  FlatList,
  StatusBar,
} from 'react-native';
import {faker} from '@faker-js/faker';
import {AntIcon} from './vectorIconsPortFull';
import Content from './src/components/Content';
import images from './src/constants/images';

const {width, height} = Dimensions.get('screen');
const IMAGE_WIDTH = width * 0.65;
const IMAGE_HEIGHT = IMAGE_WIDTH * 0.7;

faker.seed(13);

const DATA = [...Array(images.length).keys()].map((_, i) => {
  return {
    key: faker.datatype.uuid(),
    image: images[i].src,
    title: faker.commerce.productName(),
    subtitle: faker.company.bs(),
    price: faker.finance.amount(80, 200, 0),
  };
});
const SPACING = 20;

const App = () => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const progress = Animated.modulo(Animated.divide(scrollX, width), width);
  const [indexS, setIndex] = useState(0);
  const flatRef = useRef();
  console.log(indexS);
  return (
    <View style={styles.mainContainer}>
      <StatusBar hidden />
      <SafeAreaView style={styles.safearea}>
        <View style={styles.flatMainContainer}>
          <Animated.FlatList
            ref={flatRef}
            data={DATA}
            keyExtractor={item => item.key}
            horizontal
            pagingEnabled
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {x: scrollX}}}],
              {useNativeDriver: true},
            )}
            bounces={false}
            style={styles.flat}
            contentContainerStyle={styles.flatContContainer}
            onMomentumScrollEnd={ev => {
              setIndex(Math.floor(ev.nativeEvent.contentOffset.x / width));
            }}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => {
              const inputRange = [
                (index - 1) * width,
                index * width,
                (index + 1) * width,
              ];
              const opacity = scrollX.interpolate({
                inputRange,
                outputRange: [0, 1, 0],
              });
              const translateY = scrollX.interpolate({
                inputRange,
                outputRange: [50, 0, 20],
              });
              return (
                <Animated.View
                  style={{
                    ...styles.renderItemMain,
                    opacity,
                    transform: [{translateY}],
                  }}>
                  <Image
                    source={item.image}
                    //source={item.src}
                    style={styles.renderItemImage}
                  />
                </Animated.View>
              );
            }}
          />
          <View style={styles.contentMain}>
            {DATA.map((item, index) => {
              const inputRange = [
                (index - 0.2) * width,
                index * width,
                (index + 0.2) * width,
              ];
              const opacity = scrollX.interpolate({
                inputRange,
                outputRange: [0, 1, 0],
              });
              const rotateY = scrollX.interpolate({
                inputRange,
                outputRange: ['45deg', '0deg', '45deg'],
              });
              return (
                <Animated.View
                  key={item.key}
                  style={{
                    ...styles.contentMapView,
                    opacity,
                    transform: [
                      {
                        perspective: IMAGE_WIDTH * 4,
                      },
                      {
                        rotateY,
                      },
                    ],
                  }}>
                  <Content item={item} SPACING={SPACING} />
                </Animated.View>
              );
            })}
          </View>
          <Animated.View
            style={{
              ...styles.afterContent,
              transform: [
                {
                  perspective: IMAGE_WIDTH * 4,
                },
                {
                  rotateY: progress.interpolate({
                    inputRange: [0, 0.5, 1],
                    outputRange: ['0deg', '90deg', '180deg'],
                  }),
                },
              ],
            }}
          />
        </View>
        <View style={styles.buttonsMain}>
          <Pressable
            disabled={indexS === 0}
            style={{opacity: indexS === 0 ? 0.3 : 1}}
            onPress={() => {
              flatRef?.current?.scrollToOffset({
                offset: (indexS - 1) * width,
                animated: true,
              });
            }}>
            <View style={styles.pressable}>
              <AntIcon name="swapleft" size={42} color="black" />
              <Text style={styles.pressText}>PREV</Text>
            </View>
          </Pressable>
          <Pressable
            disabled={indexS === DATA.length - 1}
            style={{opacity: indexS === DATA.length - 1 ? 0.3 : 1}}
            onPress={() => {
              flatRef?.current?.scrollToOffset({
                offset: (indexS + 1) * width,
                animated: true,
              });
            }}>
            <View style={styles.pressable}>
              <Text style={styles.pressText}>NEXT</Text>
              <AntIcon name="swapright" size={42} color="black" />
            </View>
          </Pressable>
        </View>
      </SafeAreaView>
    </View>
  );
};
export default App;

const styles = StyleSheet.create({
  mainContainer: {backgroundColor: '#A5F1FA', flex: 1},
  safearea: {marginTop: SPACING * 4},
  flatMainContainer: {height: IMAGE_HEIGHT * 2.1},
  flat: {flexGrow: 0},
  flatContContainer: {
    height: IMAGE_HEIGHT + SPACING * 2,
    paddingHorizontal: SPACING * 2,
  },
  renderItemMain: {
    width,
    paddingVertical: SPACING,
  },
  renderItemImage: {
    width: IMAGE_WIDTH,
    height: IMAGE_HEIGHT,
    resizeMode: 'cover',
  },
  contentMain: {
    width: IMAGE_WIDTH,
    alignItems: 'center',
    paddingHorizontal: SPACING * 2,
    marginLeft: SPACING * 2,
    zIndex: 99,
  },
  contentMapView: {
    position: 'absolute',
  },
  afterContent: {
    width: IMAGE_WIDTH + SPACING * 2,
    position: 'absolute',
    backgroundColor: 'white',
    //backfaceVisibility: true,
    zIndex: -1,
    top: SPACING * 2,
    left: SPACING,
    bottom: 0,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 24,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    elevation: 2,
  },
  buttonsMain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: IMAGE_WIDTH + SPACING * 4,
    paddingHorizontal: SPACING,
    paddingVertical: SPACING,
  },
  pressable: {flexDirection: 'row', alignItems: 'center'},
  pressText: {fontSize: 12, fontWeight: '800'},
});
