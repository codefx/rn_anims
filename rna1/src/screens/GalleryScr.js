import React, {useRef, useState} from 'react';
import {
  StatusBar,
  FlatList,
  Image,
  Animated,
  Text,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Easing,
  SafeAreaViewBase,
  SafeAreaView,
  Pressable,
} from 'react-native';
import images from '../constants/images';
const {width, height} = Dimensions.get('screen');
const IMG_SIZE = 80;
const SPACING = 10;

const GalleryScr = () => {
  const bigFlat = useRef();
  const smallFlat = useRef();
  const [activeIndex, setActiveIndex] = useState(0);

  const scrollToActiveIndex = index => {
    setActiveIndex(index);
    bigFlat?.current?.scrollToOffset({
      offset: index * width,
      animated: true,
    });
    if (index * (IMG_SIZE + SPACING) - IMG_SIZE / 2 > width / 2) {
      smallFlat?.current?.scrollToOffset({
        offset: index * (IMG_SIZE + SPACING) - width / 2 + IMG_SIZE / 2,
        animated: true,
      });
    } else {
      smallFlat?.current?.scrollToOffset({
        offset: 0,
        animated: true,
      });
    }
  };
  return (
    <View style={styles.main}>
      <FlatList
        ref={bigFlat}
        data={images}
        keyExtractor={item => item.id.toString()}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={ev => {
          scrollToActiveIndex(
            Math.round(ev.nativeEvent.contentOffset.x / width),
          );
        }}
        renderItem={({item}) => {
          return (
            <View style={{width, height}}>
              <Image source={item.src} style={{width, height}} />
            </View>
          );
        }}
      />
      <FlatList
        ref={smallFlat}
        data={images}
        keyExtractor={item => item.id.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.smallFlat}
        contentContainerStyle={{paddingHorizontal: SPACING}}
        renderItem={({item, index}) => {
          return (
            <Pressable onPress={() => scrollToActiveIndex(index)}>
              <Image
                source={item.src}
                style={{
                  ...styles.smallImg,
                  borderColor: activeIndex === index ? '#fff' : 'transparent',
                }}
              />
            </Pressable>
          );
        }}
      />
    </View>
  );
};

export default GalleryScr;

const styles = StyleSheet.create({
  main: {flex: 1, backgroundColor: '#000000'},
  smallImg: {
    width: IMG_SIZE,
    height: IMG_SIZE,
    borderRadius: 12,
    marginRight: SPACING,
    borderWidth: 2,
  },
  smallFlat: {
    position: 'absolute',
    bottom: IMG_SIZE,
  },
});
