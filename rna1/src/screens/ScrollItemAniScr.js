import React, {useRef} from 'react';
import {
  StatusBar,
  FlatList,
  Image,
  Animated,
  Text,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Easing,
  SafeAreaViewBase,
  SafeAreaView,
} from 'react-native';
const {width, height} = Dimensions.get('screen');
import {faker} from '@faker-js/faker';

faker.seed(10);
const DATA = [...Array(30).keys()].map((_, i) => {
  return {
    key: faker.datatype.uuid(),
    image: `https://randomuser.me/api/portraits/${faker.helpers.arrayElement([
      'women',
      'men',
    ])}/${faker.random.numeric(2)}.jpg`,
    name: faker.name.fullName(),
    jobTitle: faker.name.jobTitle(),
    email: faker.internet.email(),
  };
});

const BG_IMG =
  'https://images.pexels.com/photos/1231265/pexels-photo-1231265.jpeg?auto=compress';
const SPACING = 20;
const AVATAR_SIZE = 70;
const ITEM_SIZE = AVATAR_SIZE + SPACING * 3;
const ScrollItemAniScr = () => {
  const scrollY = useRef(new Animated.Value(0)).current;
  return (
    <View style={styles.main}>
      <Image
        source={{uri: BG_IMG}}
        style={StyleSheet.absoluteFillObject}
        blurRadius={80}
      />
      <Animated.FlatList
        data={DATA}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: true},
        )}
        keyExtractor={item => item.key}
        contentContainerStyle={styles.flatlist}
        renderItem={({item, index}) => {
          const inputRange = [
            -1,
            0,
            ITEM_SIZE * index,
            ITEM_SIZE * (index + 2),
          ];
          const opacityInputRange = [
            -1,
            0,
            ITEM_SIZE * index,
            ITEM_SIZE * (index + 0.5),
          ];
          const scale = scrollY.interpolate({
            inputRange,
            outputRange: [1, 1, 1, 0],
          });
          const opacity = scrollY.interpolate({
            inputRange: opacityInputRange,
            outputRange: [1, 1, 1, 0],
          });
          return (
            <Animated.View
              style={{
                ...styles.renderItem,
                opacity,
                transform: [{scale}],
              }}>
              <Image source={{uri: item.image}} style={styles.image} />
              <View>
                <Text style={styles.text1}>{item.name}</Text>
                <Text style={styles.text2}>{item.jobTitle}</Text>
                <Text style={styles.text3}>{item.email}</Text>
              </View>
            </Animated.View>
          );
        }}
      />
    </View>
  );
};

export default ScrollItemAniScr;

const styles = StyleSheet.create({
  main: {flex: 1, backgroundColor: '#fff'},
  flatlist: {
    padding: SPACING,
    paddingTop: StatusBar.currentHeight || 42,
  },
  renderItem: {
    flexDirection: 'row',
    elevation: 1,
    borderRadius: 16,
    backgroundColor: 'rgba(255,255,255,0.8)',
    padding: SPACING * 0.6,
    marginBottom: SPACING * 0.6,
  },

  image: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE,
    marginRight: SPACING / 2,
  },
  text1: {
    fontSize: 18,
    fontWeight: '700',
  },
  text2: {
    fontSize: 14,
    opacity: 0.7,
  },
  text3: {
    fontSize: 12,
    opacity: 0.8,
    color: '#3dbdf4',
  },
});
