import React, {useRef} from 'react';
import {
  StatusBar,
  FlatList,
  Image,
  Animated,
  Text,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import images from '../constants/images';
const {width, height} = Dimensions.get('screen');

const imageW = width * 0.7;
const imageH = imageW * 1.54;
const BlurredCarousel = () => {
  const scrollX = useRef(new Animated.Value(0)).current;
  return (
    <View style={styles.main}>
      <StatusBar hidden />
      <View style={StyleSheet.absoluteFillObject}>
        {images.map((image, index) => {
          const inputRange = [
            (index - 1) * width, //next
            index * width, //current
            (index + 1) * width, //previous
          ];
          const opacity = scrollX.interpolate({
            inputRange,
            outputRange: [0, 1, 0],
          });
          return (
            <Animated.Image
              key={`image-${index}`}
              source={image.src}
              style={[StyleSheet.absoluteFillObject, {opacity}]}
              blurRadius={24}
            />
          );
        })}
      </View>
      <Animated.FlatList
        data={images}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: true},
        )}
        keyExtractor={(item, index) => index.toString()}
        horizontal
        pagingEnabled
        renderItem={({item}) => {
          return (
            <View style={styles.renderItemMain}>
              <Image source={item.src} style={styles.renderItemImage} />
            </View>
          );
        }}
      />
    </View>
  );
};

export default BlurredCarousel;

const styles = StyleSheet.create({
  main: {flex: 1, backgroundColor: '#000'},
  renderItemMain: {
    elevation: 2,
    width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  renderItemImage: {
    width: imageW,
    height: imageH,
    resizeMode: 'cover',
    borderRadius: 16,
  },
});
