import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  Pressable,
  View,
  StyleSheet,
} from 'react-native';
import BlurredCarousel from './src/screens/BlurredCarousel';
import GalleryScr from './src/screens/GalleryScr';
import ScrollItemAniScr from './src/screens/ScrollItemAniScr';

const App = () => {
  return (
    <>
      {/* <ScrollItemAniScr /> */}
      {/* <GalleryScr /> */}
      <BlurredCarousel />
    </>
  );
};
export default App;

const styles = StyleSheet.create({
  main: {flex: 1, backgroundColor: '#fff'},
});
